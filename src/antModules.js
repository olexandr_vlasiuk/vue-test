import Vue from 'vue'
import {
  Col,
  Row,
  Menu,
  Input,
  Button,
  Layout,
  Avatar,
  Popover,
  Badge,
  Select,
  Icon
} from 'ant-design-vue'

Vue.component(Col.name, Col)
Vue.component(Row.name, Row)

Vue.component(Avatar.name, Avatar)

Vue.component(Layout.name, Layout)
Vue.component(Layout.Header.name, Layout.Header)
Vue.component(Layout.Content.name, Layout.Content)

Vue.component(Menu.name, Menu)
Vue.component(Menu.Item.name, Menu.Item)

Vue.component(Input.name, Input)
Vue.component(Input.Search.name, Input.Search)
Vue.component(Button.name, Button)
Vue.component(Select.name, Select)
Vue.component(Select.Option.name, Select.Option)

Vue.component(Popover.name, Popover)

Vue.component(Badge.name, Badge)
Vue.component(Icon.name, Icon)
