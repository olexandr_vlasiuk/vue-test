// initial state
const state = {
  list: [
    {
      id: 1,
      item: 'Test 1'
    },
    {
      id: 2,
      item: 'Test 2'
    }
  ]
}

// getters
const getters = {
  getAllList () {
    return state.list
  }
}

// actions
const actions = {
  addItem ({ state, commit }, item) {
    if (item) {
      commit('pushItemToStore', { id: state.list.length + 1, item })
    }
  }
}

// mutations
const mutations = {
  pushItemToStore (state, item) {
    state.list.push(item)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
