import Vue from 'vue'
import './AntModules'
import store from './store'
import App from './App.vue'
import router from './router'
import 'ant-design-vue/dist/antd.css'
import './main.styl'
import './assets/css/swiper.css'

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
